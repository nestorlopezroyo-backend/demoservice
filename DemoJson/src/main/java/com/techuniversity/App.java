package com.techuniversity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
        String resp = "";
        try{
            while (!resp.equals("S")){
                resp = entrada.readLine();
                if(!resp.equals("S")){
                    int id = Integer.parseInt(resp);
                    JSONObject selUser = Users.getUser(id);
                    mostrarUser(selUser);
                }
            }
        } catch (Exception ex) {System.out.println(ex.getMessage());}
    }

    public static void mostrarUser(JSONObject user) throws Exception {
        if (user != null) {
            System.out.println(user.getInt("userId"));
            System.out.println(user.getString("firstName"));
            System.out.println(user.getString("lastName"));
            System.out.println(user.getString("phoneNumber"));
            System.out.println(user.getString("emailAddress"));
        } else {
            System.out.println("Usuario no encontrado.");
        }
    }
}
